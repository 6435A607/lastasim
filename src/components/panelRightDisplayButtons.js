import React from 'react';
import MonitorButton from './monitorButton';
import styles from './styleMonitor.module.css';

export default function PanelRightDisplayButtons(props) {
  return(
    <div className={styles.panelSideButtons}>
      <MonitorButton label='&#11176;'/>
      <MonitorButton label='&#11013;'/>
      <MonitorButton label='&#10145;'/>
      <MonitorButton label='&#11015;'/>
      <MonitorButton label='&#11014;'/>
    </div>
  )
}