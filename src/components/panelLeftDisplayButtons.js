import React from 'react';
import MonitorButton from './monitorButton';
import styles from './styleMonitor.module.css';

export default function PanelLeftDisplayButtons(props) {
  return (
    <div className={styles.panelSideButtons}>
      <MonitorButton label='&#9675;' />
      <MonitorButton label='&#9898;' />
    </div>
  )
}