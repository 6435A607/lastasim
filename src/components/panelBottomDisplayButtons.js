import React from 'react';
import MonitorButton from './monitorButton';
import styles from './styleMonitor.module.css';

export default function PanelBottomDisplayButtons(props) {
  let buttons = [];
  let amountButtons = 10;
  for(let i = 1; i < amountButtons + 1; i++) {
    buttons.push(<MonitorButton  
                  key={i} href={i-1%amountButtons} //Цифры от 0 до 9
                  label={i%amountButtons} />); //Цифры от 1 до 0
  }
  return (
    <div className={styles.panelBottomButtons}>
      {buttons}
    </div>
  )
}

