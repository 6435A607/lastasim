export function currentDate() {
  let date = new Date();
  let year = date.getFullYear();
  let month = date.getMonth()+1;
  let day = date.getDate();
  return `${ day < 10 ? `0${day}`: day}.${ month < 10 ? `0${month}`: month}.${year}`;
}

export function currentTime() {
  let date = new Date();
  let hour = date.getHours();
  let minute = date.getMinutes();
  return `${hour}:${minute < 10 ? `0${minute}` : minute}`;
}