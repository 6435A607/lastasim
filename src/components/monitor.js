import React from 'react';
import PanelLeftDisplayButtons from './panelLeftDisplayButtons';
import PanelRightDisplayButtons from './panelRightDisplayButtons';
import Screen from './screens/screen';
import PanelBottomDisplayButtons from './panelBottomDisplayButtons';
import { withRouter, Switch, Route, Redirect } from 'react-router-dom';
import stylesScreen from './screens/styleScreen.module.css';
import styleMonitor from './styleMonitor.module.css';

function Monitor(props) {
  return (
    <div className={styleMonitor.monitor}>
      <div className={styleMonitor.generalPanel}>
        <PanelLeftDisplayButtons />
        <div className={styleMonitor.display}>
          <div className={styleMonitor.recess}>
            <div className={stylesScreen.Image}>
              <Switch>
                <Route path='/display/:id?' component={Screen} />
                <Redirect from='/' to='/display/Main screen' />
              </Switch>
            </div>
          </div>
        </div>
        <PanelRightDisplayButtons />
      </div>
      <PanelBottomDisplayButtons />
    </div>
  );
}

export default withRouter(Monitor);