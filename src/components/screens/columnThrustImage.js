import React from 'react';

export default function ColumnThrustImage(props) {
  return (
    <>
      <svg
        width={'18vw'}
        height={'18vw'}
        viewBox="0 0 116.441 141.611"
        xmlns="http://www.w3.org/2000/svg"
        {...props}
      >
        <g transform="translate(-14 -25.812)">
          <path fill="#b3b3b3" fillRule="evenodd" d="M36.879 36.35h30v120h-30z" />
          <path
            d="M68.88 36.35H34.878v0M68.88 46.49h-34v0M68.88 56.49h-34v0M68.88 66.49h-34v0M68.88 76.49h-34v0M68.88 86.49h-34v0M68.88 96.49h-34v0M68.88 106.49h-34v0M68.88 116.49h-34v0M68.88 126.49h-34v0M68.88 136.49h-34v0M68.88 146.49h-34v0M68.88 156.49h-34v0"
            fill="#fff"
            stroke="#fff"
            strokeWidth={0.282}
          />
          <path fill="#b3b3b3" fillRule="evenodd" d="M66.879 36.35h30v120h-30z" />
          <path
            d="M98.879 36.35h-32v0M98.879 46.49h-32v0M98.879 56.49h-32v0M98.879 66.491h-32v0M98.879 76.491h-32v0M98.879 86.491h-32v0M98.879 96.491h-32v0M98.879 106.491h-32v0M98.879 116.491h-32v0M98.879 126.491h-32v0M98.879 136.491h-32v0M98.879 146.491h-32v0M98.879 156.491h-32v0"
            fill="#fff"
            stroke="#fff"
            strokeWidth={0.273}
          />
          <text
            style={{
              lineHeight: 1.25,
            }}
            x={102.177}
            y={39.572}
            fontWeight={400}
            fontSize={7.761}
            fontFamily="sans-serif"
            fill="#fff"
            strokeWidth={0.265}
          >
            <tspan x={102.177} y={39.572}>
              {"+100%"}
            </tspan>
          </text>
          <text
            style={{
              lineHeight: 1.25,
            }}
            x={102.621}
            y={157.572}
            fontWeight={400}
            fontSize={7.761}
            fontFamily="sans-serif"
            fill="#fff"
            strokeWidth={0.265}
          >
            <tspan x={102.621} y={157.572}>
              {"-100%"}
            </tspan>
          </text>
          <text
            style={{
              lineHeight: 1.25,
            }}
            x={102.488}
            y={98.572}
            fontWeight={400}
            fontSize={7.761}
            fontFamily="sans-serif"
            fill="#fff"
            strokeWidth={0.265}
          >
            <tspan x={102.488} y={98.572}>
              {"0%"}
            </tspan>
          </text>
          <a>
            <text
              style={{
                lineHeight: 1.25,
              }}
              x={13.488}
              y={98.619}
              fontWeight={400}
              fontSize={7.761}
              fontFamily="sans-serif"
              fill="#fff"
              strokeWidth={0.265}
            >
              <tspan x={13.488} y={98.619}>
                {"0%"}
              </tspan>
            </text>
          </a>
          <path d="M28.935 100.547l-.056-8 6 3.944z" fill="#fff" />
          <text
            style={{
              lineHeight: 1.25,
            }}
            x={38.02}
            y={30.955}
            fontWeight={400}
            fontSize={7.056}
            fontFamily="sans-serif"
            fill="#fff"
            strokeWidth={0.265}
          >
            <tspan x={38.02} y={30.955}>
              {"Тяговые усилия"}
            </tspan>
          </text>
          <text
            style={{
              lineHeight: 1.25,
            }}
            x={34.02}
            y={165.955}
            fontWeight={400}
            fontSize={7.056}
            fontFamily="sans-serif"
            fill="#fff"
            strokeWidth={0.265}
          >
            <tspan x={37.02} y={165.955}>
              {"Тормозные усилия"}
            </tspan>
          </text>
        </g>
      </svg>
    </>
  )
};
