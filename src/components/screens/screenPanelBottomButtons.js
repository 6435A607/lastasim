import React from 'react';
import ScreenButton from './screenButton';
import { listDisplays } from '../redux/dataScreen';
import { withRouter } from 'react-router-dom';
import styles from './styleScreen.module.css';

function ScreenPanelBottomButtons(props) {
  let type = props.idDisplay;
  let data = listDisplays[type] || [];
  let buttons = data.map((e,i) => {
    return <ScreenButton key={i} title={e.name} number={ e.id? (i+1)%10: e.id} />});

  return (
    <div className={styles.PanelBottomButtons}>
      {buttons}
    </div>
  )
};

export default withRouter(ScreenPanelBottomButtons);