import React from 'react';
import styles from './styleScreenSettings.module.css';
import ItemNavSettings from './itemNavSettings';

export default function ScreenNavPanelSettings(props) {
  let navData = props.data || null;
  let navItems = navData.map((e, i) => {
    return <ItemNavSettings key={i} name={e.name} button={e.button} />
  });

  return(
    <>
      <div className={styles.navPanel}>
          {navItems}
      </div>
    </>
  )
}