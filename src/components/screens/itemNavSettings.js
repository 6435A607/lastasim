import React from 'react';
import style from './styleScreenSettings.module.css';

export default function ItemNavSettings(props) {
  let name = props.name || null;
  let button = props.button || null;
  return (
    <>
      <div className={style.navItem}>
        <div className={style.navName}>{name}</div>
        <div className={style.navButton}>{button}</div>
      </div>
    </>
  )
}