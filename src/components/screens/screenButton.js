import React from 'react';
import styles from './styleScreen.module.css';

export default function ScreenButton(props) {
  let title = props.title;
  let number = props.number;
  return (
    <>
      <div className={styles.Button}>
        <div className='title'>{title}</div>
        <div className='number'>{number}</div>
      </div>
    </>
  )
};
