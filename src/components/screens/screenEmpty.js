import React from 'react';
import ScreenNavPanelSettings from './screenNavPanelSettings';
import { navPanelSettings } from '../redux/dataScreen';

export function ScreenEmpty(props) {
  return (
    <>
      <div className='MonitorScreenArticle'></div>
      <ScreenNavPanelSettings data={navPanelSettings} />
    </>
  )
}