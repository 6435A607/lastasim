import React from 'react';

export default function ColumnVoltageImage(props) {
  return (
    <>
      <svg
        width={'9vw'}
        height={'18vw'}
        viewBox="0 0 48.259 138.073"
        xmlns="http://www.w3.org/2000/svg"
        {...props}
      >
        <path fill="#b3b3b3" fillRule="evenodd" d="M2 10.256h30v120H2z" />
        <path
          d="M34 10.256H0h0M34 20.397H0h0M34 30.397H0h0M34 40.397H0h0M34 50.397H0h0M34 60.397H0h0M34 70.397H0h0M34 80.397H0h0M34 90.397H0h0M34 100.397H0h0M34 110.397H0h0M34 120.397H0h0M34 130.397H0h0"
          fill="#fff"
          stroke="#fff"
          strokeWidth={0.282}
        />
        <path fill="#0000fe" d="M2 40.115h30v90H2z" />
        <text
          style={{
            lineHeight: 1.25,
          }}
          x={92.753}
          y={162.619}
          fontWeight={400}
          fontSize={7.761}
          fontFamily="sans-serif"
          fill="#fff"
          strokeWidth={0.265}
          transform="translate(-56.264 -29.744)"
        >
          <tspan x={92.753} y={162.619}>
            {"0"}
          </tspan>
        </text>
        <text
          style={{
            lineHeight: 1.25,
          }}
          x={92.411}
          y={132.619}
          fontWeight={400}
          fontSize={7.761}
          fontFamily="sans-serif"
          fill="#fff"
          strokeWidth={0.265}
          transform="translate(-56.264 -29.744)"
        >
          <tspan x={92.411} y={132.619}>
            {"1.0"}
          </tspan>
        </text>
        <text
          style={{
            lineHeight: 1.25,
          }}
          x={92.696}
          y={102.619}
          fontWeight={400}
          fontSize={7.761}
          fontFamily="sans-serif"
          fill="#fff"
          strokeWidth={0.265}
          transform="translate(-56.264 -29.744)"
        >
          <tspan x={92.696} y={102.619}>
            {"2.0"}
          </tspan>
        </text>
        <text
          style={{
            lineHeight: 1.25,
          }}
          x={92.673}
          y={72.619}
          fontWeight={400}
          fontSize={7.761}
          fontFamily="sans-serif"
          fill="#fff"
          strokeWidth={0.265}
          transform="translate(-56.264 -29.744)"
        >
          <tspan x={92.673} y={72.619}>
            {"3.0"}
          </tspan>
        </text>
        <text
          style={{
            lineHeight: 1.25,
          }}
          x={66.325}
          y={35.402}
          fontWeight={400}
          fontSize={7.761}
          fontFamily="sans-serif"
          fill="#fff"
          strokeWidth={0.265}
          transform="translate(-56.264 -29.744)"
        >
          <tspan x={66.325} y={35.402}>
            {"U\u043A\u0441"}
          </tspan>
        </text>
        <text
          style={{
            lineHeight: 1.25,
          }}
          x={61.408}
          y={167.707}
          fontWeight={400}
          fontSize={7.761}
          fontFamily="sans-serif"
          fill="#fff"
          strokeWidth={0.265}
          transform="translate(-56.264 -29.744)"
        >
          <tspan x={61.408} y={167.707}>
            {`${props.voltage} \u043A\u0412`}
          </tspan>
        </text>
      </svg>
    </>
  )
};
