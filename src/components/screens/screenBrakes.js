import React from 'react';
import { listBrakingDevices } from '../redux/dataScreen';
import ItemPowerDevice from './itemPowerDevice';
import styles from './styleListOfDevices.module.css';
import ScreenStateTrain from './screenStateTrain';
import ScreenPanelBottomButtons from './screenPanelBottomButtons';

export default function ScreenBrakes(props) {
  let data = listBrakingDevices;
  let items = data.map((e, i) => {
    return <ItemPowerDevice name={e.id} key={i}
      front={e.data.front}
      center={e.data.center}
      rear={e.data.rear} />
  });
  return (
    <>
      <div className='MonitorScreenArticle'>
        <div className={styles.listOfDevices}>
          <ItemPowerDevice center={[{ value: 'Поезд 1' }]} />
          {items}
        </div>
      </div>
      <ScreenStateTrain />
      <ScreenPanelBottomButtons idDisplay={props.idDisplay} />
    </>
  )
}