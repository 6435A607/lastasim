import React from 'react';
import { listSettings, navPanelSettings } from '../redux/dataScreen';
import ItemMode from './itemMode';
import styles from './styleListOfDevices.module.css';
import { withRouter } from 'react-router-dom';
import ScreenNavPanelSettings from './screenNavPanelSettings';

function ScreenSettings(props) {
  let data = listSettings;
  let items = data.map((e, index) => {
    return <ItemMode name={e.name} key={index} status={e} />
  });

  return (
    <>
      <div className='MonitorScreenArticle'>
        <div className={styles.listMode}>
          {items}
        </div>
      </div>
      <ScreenNavPanelSettings data={navPanelSettings} />
    </>
  )
}

export default withRouter(ScreenSettings);