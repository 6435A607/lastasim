import React from 'react';
import IconPowerDevice from './iconPowerDevice';
import style from './styleTrainImage.module.css';
import TrainWagon from './trainWagon';

export default function Train(props) {
  let { cabin, wagons } = props;
  let numberOfWagon = [
    '02101',
    '02102',
    '02103',
    '02104',    
    '02105',
  ]
  let itemsWagons = numberOfWagon.map((e, i) => <TrainWagon key={i} data={wagons[i]} number={e}/>);

  return (
    <>
      <div className={style.imageTrain}>
        <div className={style.edge}></div>
        <div className={style.cabin }>
          {cabin.front.map((e,i)=> <IconPowerDevice data={e} key={i} />)}         
        </div>     
           
        {itemsWagons}

        <div className={style.cabin}>
          {cabin.rear.map((e,i)=> <IconPowerDevice data={e} key={i} />)}         
        </div>
        <div className={style.edge}></div>
      </div>
    </>
  )
}

Train.defaultProps = {
  cabin: {
    front:[],
    rear:[]
  },
  wagons: [ {}, {}, {}, {}, {}],
}