import React from 'react';

export default function ColumnAmperageImage(props) {
  return (
    <>
      <svg
        width={'9vw'}
        height={'18vw'}
        viewBox="0 0 55.637 140.87"
        xmlns="http://www.w3.org/2000/svg"
        {...props}
      >
        <path fill="#b3b3b3" fillRule="evenodd" d="M2 12.14h30v120H2z" />
        <path
          d="M34 12.14H0h0M34 22.282H0h0M34 32.282H0h0M34 42.282H0h0M34 52.282H0h0M34 62.282H0h0M34 72.282H0h0M34 82.282H0h0M34 92.282H0h0M34 102.282H0h0M34 112.282H0h0M34 122.282H0h0M34 132.282H0h0"
          fill="#fff"
          stroke="#fff"
          strokeWidth={0.282}
        />
        <text
          style={{
            lineHeight: 1.25,
          }}
          x={64.488}
          y={152.619}
          fontWeight={400}
          fontSize={7.761}
          fontFamily="sans-serif"
          fill="#fff"
          strokeWidth={0.265}
          transform="translate(-28 -17.86)"
        >
          <tspan x={64.488} y={152.619}>
            {"0"}
          </tspan>
        </text>
        <text
          style={{
            lineHeight: 1.25,
          }}
          x={64.488}
          y={102.619}
          fontWeight={400}
          fontSize={7.761}
          fontFamily="sans-serif"
          fill="#fff"
          strokeWidth={0.265}
          transform="translate(-28 -17.86)"
        >
          <tspan x={64.488} y={102.619}>
            {"500"}
          </tspan>
        </text>
        <text
          style={{
            lineHeight: 1.25,
          }}
          x={64.401}
          y={52.619}
          fontWeight={400}
          fontSize={7.761}
          fontFamily="sans-serif"
          fill="#fff"
          strokeWidth={0.265}
          transform="translate(-28 -17.86)"
        >
          <tspan x={64.401} y={52.619}>
            {"1000"}
          </tspan>
        </text>
        <text
          style={{
            lineHeight: 1.25,
          }}
          x={39.238}
          y={23.517}
          fontWeight={400}
          fontSize={7.761}
          fontFamily="sans-serif"
          fill="#fff"
          strokeWidth={0.265}
          transform="translate(-28 -17.86)"
        >
          <tspan x={39.238} y={23.517}>
            {"I\u043A\u0441"}
          </tspan>
        </text>
        <text
          style={{
            lineHeight: 1.25,
          }}
          x={39.488}
          y={158.619}
          fontWeight={400}
          fontSize={7.761}
          fontFamily="sans-serif"
          fill="#fff"
          strokeWidth={0.265}
          transform="translate(-28 -17.86)"
        >
          <tspan x={39.488} y={158.619}>
            {`${props.amperage} A`}
          </tspan>
        </text>
      </svg>
      )
    }
    </>
  )
};
