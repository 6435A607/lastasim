import React from 'react';
import {listClimateDevices} from '../redux/dataScreen';
import Train from './train';
import ScreenStateTrain from './screenStateTrain';
import ScreenPanelBottomButtons from './screenPanelBottomButtons';

export default function ScreenClimate(props) {
  let data = listClimateDevices;
  
  return (
    <>
      <div className='MonitorScreenArticle'>
        <Train cabin={data.cabin} wagons = {data.wagons} />   
      </div>
      <ScreenStateTrain />
      <ScreenPanelBottomButtons idDisplay = {props.idDisplay} />
    </>
  )
}