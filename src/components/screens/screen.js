import React from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import ScreenHeader from './screenHeader';
import ScreenMain from './screenMain';
import ScreenDoors from './screenDoors';
import ScreenBrakes from './screenBrakes';
import ScreenPowerCircuit from './screenPowerCircuit';
import ScreenClimate from './screenClimate';
import ScreenAxlebox from './screenAxlebox';
import ScreenSettings from './screenSettings';
import ScreenAdditionalInformation from './screenAdditionalInformation';
import { ScreenEmpty } from './screenEmpty';

function Screen(props) {
  let idDisplay = props.location.pathname.replaceAll('/display/', '');
  return (
    <>
      <ScreenHeader idDisplay={idDisplay} />
      <Switch>
        <Route path='/display/Main screen' render={() => <ScreenMain idDisplay={idDisplay} />} />
        <Route path='/display/Doors' render={() => <ScreenDoors idDisplay={idDisplay} />} />
        <Route path='/display/Brakes' render={() => <ScreenBrakes idDisplay={idDisplay} />} />
        <Route path='/display/Power Circuit' render={() => <ScreenPowerCircuit idDisplay={idDisplay} />} />
        <Route path='/display/Climate' render={() => <ScreenClimate idDisplay={idDisplay} />} />
        <Route path='/display/Axlebox' render={() => <ScreenAxlebox idDisplay={idDisplay} />} />
        <Route path='/display/Additional Information' render={() => <ScreenAdditionalInformation idDisplay={idDisplay} />} />
        <Route path='/display/Settings' render={() => <ScreenSettings idDisplay={idDisplay} />} />
        <Route path='/display/SIM' render={()=><ScreenEmpty idDisplay={idDisplay}/>}/>
      </Switch>

    </>
  )
}

export default withRouter(Screen);