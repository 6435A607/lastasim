import React from 'react';
import train from './images/train.svg';
export default function TrainImage(props) {
  return (
    <>
      <img src={train} />
    </>
  );
}
