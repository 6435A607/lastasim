import React from 'react';
import { listAxlebox, listLegendAxle } from '../redux/dataScreen';
import Train from './train';
import styles from './styleScreen.module.css';
import IconPowerDevice from './iconPowerDevice';
import ScreenStateTrain from './screenStateTrain';
import ScreenPanelBottomButtons from './screenPanelBottomButtons';

export default function ScreenAxlebox(props) {
  let data = listAxlebox;
  let dataLegend = listLegendAxle.map((e, i) => {
    return <div key={i}><IconPowerDevice data={e} /><div>{e.description}</div></div>
  });

  return (
    <>
    <div className='MonitorScreenArticle'>
      <Train wagons={data.wagons} />
      <div className={styles.legendAxle}>
        {dataLegend}
      </div>
    </div>
    <ScreenStateTrain />
    <ScreenPanelBottomButtons idDisplay = {props.idDisplay} />
    </>
  )
}