import React from 'react';
import IconPowerDevice from './iconPowerDevice';
import style from './styleTrainImage.module.css';

export default function TrainWagon(props) {
  let top = props.data.top || {
    front: [],
    rear: []
  }; 
  let center = props.data.center || [];
  let bottom = props.data.bottom || {
    front: [],
    rear: []
  }; 

  return (
    <>
      <div className={style.wagon}>
        <div className={style.top}>
          <div> { top.front.map((e,i) => <IconPowerDevice data={e} key={i} />) }</div>
          <div> { top.rear.map((e,i) => <IconPowerDevice data={e} key={i} />) }</div>
        </div>
        <div className={style.center}>
          {
            center.map((e,i) => <IconPowerDevice data={e} key={i} />)
          }
          <div>{props.number}</div>
        </div>
        <div className={style.bottom}>
        <div> { bottom.front.map((e,i) => <IconPowerDevice data={e} key={i} />) }</div>
        <div> { bottom.rear.map((e,i) => <IconPowerDevice data={e} key={i} />) }</div>
        </div>
      </div>
    </>
  )
}
