import React from 'react';
import IconPowerDevice from './iconPowerDevice';
import styles from './styleListOfDevices.module.css';

export default function ItemMode(props) {
  let { name, status } = props;

  return (
    <>
      <div className={styles.itemMode}>
        <div className={styles.nameMode}>{name}</div>
        <div className={styles.statusMode}>

          <div className={styles.end}>
            <IconPowerDevice data={status} />
          </div>
        </div>
      </div>
    </>
  )
}