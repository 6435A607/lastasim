import React from 'react'
import { OFF, ON, DISAPPEAR, WARNING, ERROR, BLOCK, MALFUNCTION, CHECKED } from '../redux/typesStatusDevice';
import styles from './styleListOfDevices.module.css';

export default function IconPowerDevice(props) {
  let { status, isChangeable, value, rectangle } = props.data;
  let tabIndex = -1;
  const statusStyle = () => {
    let styleStatus;
    switch (status) {
      case ON:
        styleStatus = styles.statusON;
        break;
      case OFF:
        styleStatus = styles.statusOFF;
        break;
      case WARNING:
        styleStatus = styles.statusWARNING;
        break;
      case ERROR: 
        styleStatus = styles.statusERROR;
        break;
      case BLOCK:
        styleStatus = styles.statusBLOCK;
        break;
      case MALFUNCTION:
        styleStatus = styles.statusMALFUNCTION;
        break;
      case CHECKED: 
        styleStatus = styles.statusOFF;
        value = 'v';
      break;
      case DISAPPEAR:
        debugger
        styleStatus = styles.statusDISAPPEAR;
        break;
      default: styleStatus = styles.title;
    }
    let shape = rectangle ? styles.iconOfDeviceRectangle :  styles.iconOfDevice;
    return `${shape} ${styleStatus} `;
  }
  
  let styleIsChangeable = null;
  if(isChangeable){
    styleIsChangeable = { borderColor: 'white'};
    tabIndex = 0;
  }

  return (
    <>
      <div className={statusStyle()} tabIndex={tabIndex} style={styleIsChangeable} >
        {value}
      </div>
    </>
  )
}
