import React from 'react';
import Train from './train';
import ScreenStateTrain from './screenStateTrain';
import ScreenPanelBottomButtons from './screenPanelBottomButtons';

export default function ScreenAdditionalInformation(props) {
  return (
    <>
      <div className='MonitorScreenArticle'>
        <Train  />  
      </div>
      <ScreenStateTrain />
      <ScreenPanelBottomButtons idDisplay = {props.idDisplay} />
    </>
  )
}