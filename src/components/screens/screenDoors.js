import React from 'react';
import { listDoorsData } from '../redux/dataScreen';
import Train from './train';
import ScreenStateTrain from './screenStateTrain';
import ScreenPanelBottomButtons from './screenPanelBottomButtons';

export default function ScreenDoors(props) {
  let data = listDoorsData;

  return (
    <>
      <div className='MonitorScreenArticle'>
        <Train wagons={data.wagons}/> 
      </div>
      <ScreenStateTrain />
      <ScreenPanelBottomButtons idDisplay = {props.idDisplay} />
    </>
  )
}