import React from 'react';
import styles from './styleScreen.module.css';
import ColumnVoltageImage from './columnVoltageImage';
import ColumnAmperageImage from './columnAmperageImage';
import ColumnForceImage from './columnForceImage';
import ColumnThrustImage from './columnThrustImage';
import ScreenStateTrain from './screenStateTrain';
import ScreenPanelBottomButtons from './screenPanelBottomButtons';

export default function ScreenMain(props) {
  let voltage = '3.0';
  let amperage = 0;
  let force = '0.0';

  return (
    <>
    <div className='MonitorScreenArticle'>
      <div className={styles.Columns}>
        <ColumnVoltageImage voltage={voltage} />
        <ColumnAmperageImage amperage={amperage} />
        <ColumnForceImage force={force} />
        <ColumnThrustImage />
      </div>
      <div className={styles.TrainError}>
        <div className={styles.errorMessage} style={{ visibility: 'hidden' }}>

        </div>
        <div className={styles.thrustStatus} >

        </div>
      </div>
    </div>
    <ScreenStateTrain />
    <ScreenPanelBottomButtons idDisplay = {props.idDisplay} />
    </>
  )
};
