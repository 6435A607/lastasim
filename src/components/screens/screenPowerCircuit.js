import React from 'react';
import { listPowerDevicesData } from '../redux/dataScreen';
import ItemPowerDevice from './itemPowerDevice';
import styles from './styleListOfDevices.module.css';
import ScreenStateTrain from './screenStateTrain';
import ScreenPanelBottomButtons from './screenPanelBottomButtons';

export default function ScreenPowerCircuit(props) {
  let data = listPowerDevicesData;
  let items = data.map((e, index) => {
    return <ItemPowerDevice name={e.id} key={index}
      front={e.data.front} center={e.data.center} rear={e.data.rear} />
  });

  return (
    <>
      <div className='MonitorScreenArticle'>
        <div className={styles.listOfDevices}>
          <ItemPowerDevice center={[{ value: 'Поезд 1' }]} />
          {items}
        </div>
      </div>
      <ScreenStateTrain />
      <ScreenPanelBottomButtons idDisplay={props.idDisplay} />
    </>
  )
}
