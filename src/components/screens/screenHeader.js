import React, { useState, useEffect } from 'react';
import { currentDate, currentTime } from '../date';
import styles from './styleScreen.module.css';
import { screens } from '../redux/dataScreen';

export default function ScreenHeader(props) {
  const [time, setTime] = useState(currentTime());
  let nameScreen = screens[props.idDisplay].fullName;
  let voltage = '3.0 кВ';
  let date = currentDate();

  useEffect(() => {
    let timer = setInterval(()=>setTime(currentTime), 60000);
    return () => {
      setTimeout(clearInterval(timer));
    };
  }, []);

  return (
    <div className={styles.Header}>
      <div className={styles.panelLeft}>
        <div>Поезд № 0</div>
        <div>{nameScreen}</div>
      </div>
      
      <div className={styles.panelRight}>
        <div>{voltage}</div>
        <div>{date}</div>
        <div>{time}</div>
      </div>
    </div>
  )
}