import React from 'react';

export default function ColumnForceImage(props) {
  return (
    <>
      <svg
        width={'9vw'}
        height={'18vw'}
        viewBox="0 0 50.73 140.87"
        xmlns="http://www.w3.org/2000/svg"
        {...props}
      >
        <path fill="#b3b3b3" fillRule="evenodd" d="M2 12.14h30v120H2z" />
        <path
          d="M34 12.14H0h0M34 22.282H0h0M34 32.282H0h0M34 42.282H0h0M34 52.282H0h0M34 62.282H0h0M34 72.282H0h0M34 82.282H0h0M34 92.282H0h0M34 102.282H0h0M34 112.282H0h0M34 122.282H0h0M34 132.282H0h0"
          fill="#fff"
          stroke="#fff"
          strokeWidth={0.282}
        />
        <text
          style={{
            lineHeight: 1.25,
          }}
          x={64.488}
          y={152.619}
          fontWeight={400}
          fontSize={7.761}
          fontFamily="sans-serif"
          fill="#fff"
          strokeWidth={0.265}
          transform="translate(-28 -17.86)"
        >
          <tspan x={64.488} y={152.619}>
            {"0"}
          </tspan>
        </text>
        <text
          style={{
            lineHeight: 1.25,
          }}
          x={64.488}
          y={132.619}
          fontWeight={400}
          fontSize={7.761}
          fontFamily="sans-serif"
          fill="#fff"
          strokeWidth={0.265}
          transform="translate(-28 -17.86)"
        >
          <tspan x={64.488} y={132.619}>
            {"50"}
          </tspan>
        </text>
        <text
          style={{
            lineHeight: 1.25,
          }}
          x={64.147}
          y={112.619}
          fontWeight={400}
          fontSize={7.761}
          fontFamily="sans-serif"
          fill="#fff"
          strokeWidth={0.265}
          transform="translate(-28 -17.86)"
        >
          <tspan x={64.147} y={112.619}>
            {"100"}
          </tspan>
        </text>
        <text
          style={{
            lineHeight: 1.25,
          }}
          x={64.147}
          y={92.619}
          fontWeight={400}
          fontSize={7.761}
          fontFamily="sans-serif"
          fill="#fff"
          strokeWidth={0.265}
          transform="translate(-28 -17.86)"
        >
          <tspan x={64.147} y={92.619}>
            {"150"}
          </tspan>
        </text>
        <text
          style={{
            lineHeight: 1.25,
          }}
          x={64.431}
          y={72.619}
          fontWeight={400}
          fontSize={7.761}
          fontFamily="sans-serif"
          fill="#fff"
          strokeWidth={0.265}
          transform="translate(-28 -17.86)"
        >
          <tspan x={64.431} y={72.619}>
            {"200"}
          </tspan>
        </text>
        <text
          style={{
            lineHeight: 1.25,
          }}
          x={64.431}
          y={52.619}
          fontWeight={400}
          fontSize={7.761}
          fontFamily="sans-serif"
          fill="#fff"
          strokeWidth={0.265}
          transform="translate(-28 -17.86)"
        >
          <tspan x={64.431} y={52.619}>
            {"250"}
          </tspan>
        </text>
        <text
          style={{
            lineHeight: 1.25,
          }}
          x={43.238}
          y={23.517}
          fontWeight={400}
          fontSize={7.761}
          fontFamily="sans-serif"
          fill="#fff"
          strokeWidth={0.265}
          transform="translate(-28 -17.86)"
        >
          <tspan x={43.238} y={23.517}>
            {"F"}
          </tspan>
        </text>
        <text
          style={{
            lineHeight: 1.25,
          }}
          x={32.488}
          y={158.619}
          fontWeight={400}
          fontSize={7.761}
          fontFamily="sans-serif"
          fill="#fff"
          strokeWidth={0.265}
          transform="translate(-28 -17.86)"
        >
          <tspan x={35} y={158.619}>
            {`${props.force} кН`}
          </tspan>
        </text>
      </svg>
    </>
  )
};
