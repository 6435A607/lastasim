import React from 'react';
import IconPowerDevice from './iconPowerDevice';
import styles from './styleListOfDevices.module.css';

export default function ItemPowerDevice(props) {
  let { name, front, center, rear } = props;

  return (
    <>
      <div className={styles.itemOfDevice}>
        <div className={styles.nameOfDevice}>{name}</div>
        <div className={styles.statusOfDevice}>
          <div className={styles.front}>{front.map((e, i) =>
            <IconPowerDevice data={e} key={i} />) }
          </div>
          <div className={styles.center}>
            {center.map((e, i) => <IconPowerDevice data={e} key={i} />)}
          </div>
          <div className={styles.rear}>
            {rear.map((e, i) => <IconPowerDevice data={e} key={i} />)}
          </div>
        </div>
      </div>
    </>
  )
}

ItemPowerDevice.defaultProps = {
  name: null, 
  front: [], 
  center: [], 
  rear: [],
}