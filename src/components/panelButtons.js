import React from 'react';
import MonitorButton from './monitorButton';

export default function PanelMonitorButtons(props) {
  let buttons = [];
  for(let i=0; i < props.amount; i++) {
    buttons.push(<MonitorButton key={i} />);
  }

  return (
    <div className='panel-monitor-buttons'>
      {buttons}
    </div>
  )
}