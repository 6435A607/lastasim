import { DISAPPEAR, OFF, ON, WARNING, ERROR, BLOCK, MALFUNCTION, CHECKED } from "./typesStatusDevice"

export const screens = {
  'Main screen': {
    name: 'Основн. экран',
    fullName: 'Основной экран',
    id: 'Main screen'
  },
  'Doors': {
    name: 'Двери',
    fullName: 'Состояние дверей',
    id: 'Doors'
  },
  'Brakes': {
    name: 'Тормоза',
    fullName: 'Состояние тормозов',
    id: 'Brakes'
  },
  'Power Circuit': {
    name: 'Силовая схема',
    fullName: 'Силовая схема',
    id: 'Power Circuit'
  },
  'Climate': {
    name: 'Климат',
    fullName: 'Климатическая установка',
    id: 'Climate'
  },
  'Axlebox': {
    name: 'Буксы',
    fullName: 'Контроль нагрева букс',
    id: 'Axlebox'
  },
  'Settings': {
    name: 'Настрой.',
    fullName: 'Режим поезда',
    id: 'Settings'
  },
  'Additional Information': {
    name: 'Дополн. информ.',
    fullName: 'Дополнительная информация',
    id: 'Additional Information'
  },
  'SIM': {
    name: 'СИМ',
    id: 'SIM'
  },
  'Diagnostics': {
    name: 'Диагн.',
    fullName: 'Диагностика неисправностей',
    id: 'Diagnostics'
  },
  'Legend': {
    name: 'Легенда вкл/выкл',
    id: 'Legend'
  },
  'Potel train 1': {
    name: 'Потележ поезд 1',
    id: 'Brakes'
  },
  'Potel train 2': {
    name: 'Потележ поезд 2',
    id: 'Brakes'
  },
  'Full Verification': {
    name: 'Полная проверка',
    id: 'Brakes'
  },
  'Abbreviated Verification': {
    name: 'Сокращ. проверка',
    id: 'Brakes'
  },
  'Service Screens': {
    name: 'Сервис. экраны',
    id: 'Additional Information'
  },
  'Diagnostic archive': {
    name: 'Архив диагнос.',
    id: 'Additional Information'
  },
  'Controls': {
    name: 'Элемен. управл.',
    id: 'Additional Information'
  },
  'Exit': {
    name: 'Выход',
    id: 'Main screen'
  },
  'Empty': {
    name: null,
    id: null
  }
}

export let listDisplays = {
  'Main screen': [
    screens['Main screen'],
    screens['Doors'],
    screens['Brakes'],
    screens['Power Circuit'],
    screens['Climate'],
    screens['Axlebox'],
    screens['Settings'],
    screens['Additional Information'],
    screens['SIM'],
    screens['Diagnostics'],
  ]
  ,
  'Doors': [
    screens['Main screen'],
    screens['Legend'],
    screens['Brakes'],
    screens['Power Circuit'],
    screens['Climate'],
    screens['Axlebox'],
    screens['Settings'],
    screens['Additional Information'],
    screens['SIM'],
    screens['Diagnostics'],
  ]
  ,
  'Brakes': [
    screens['Legend'],
    screens['Potel train 1'],
    screens['Potel train 1'],
    screens['Full Verification'],
    screens['Abbreviated Verification'],
    screens['Empty'],
    screens['Empty'],
    screens['Empty'],
    screens['Empty'],
    screens['Exit'],
  ],
  
  'Power Circuit': [
    screens['Main screen'],
    screens['Doors'],
    screens['Brakes'],
    screens['Legend'],
    screens['Climate'],
    screens['Axlebox'],
    screens['Settings'],
    screens['Additional Information'],
    screens['SIM'],
    screens['Diagnostics'],
  ],

  'Climate': [
    screens['Main screen'],
    screens['Doors'],
    screens['Brakes'],
    screens['Power Circuit'],
    screens['Legend'],
    screens['Axlebox'],
    screens['Settings'],
    screens['Additional Information'],
    screens['SIM'],
    screens['Diagnostics'],
  ],

  'Axlebox': [
    screens['Main screen'],
    screens['Doors'],
    screens['Brakes'],
    screens['Power Circuit'],
    screens['Climate'],
    screens['Axlebox'],
    screens['Settings'],
    screens['Additional Information'],
    screens['SIM'],
    screens['Diagnostics'],
  ],

  'Settings': [
    screens['Main screen'],
    screens['Empty'],
    screens['Empty'],
    screens['Empty'],
    screens['Empty'],
    screens['Empty'],
    screens['Empty'],
    screens['Empty'],
    screens['Empty'],
    screens['Empty'],
  ],

  'Additional Information': [
    screens['Additional Information'],
    screens['Service Screens'],
    screens['Diagnostic archive'],
    screens['Controls'],
    screens['Empty'],
    screens['Empty'],
    screens['Empty'],
    screens['Empty'],
    screens['Empty'],
    screens['Exit']
  ],

  'SIM': [
    screens['Empty'],
    screens['Empty'],
    screens['Empty'],
    screens['Empty'],
    screens['Empty'],
    screens['Empty'],
    screens['Empty'],
    screens['Empty'],
    screens['Empty'],
    screens['Exit']
  ],

  'Diagnostics': [
    screens['Empty'],
    screens['Empty'],
    screens['Empty'],
    screens['Empty'],
    screens['Empty'],
    screens['Empty'],
    screens['Empty'],
    screens['Empty'],
    screens['Empty'],
    screens['Exit']
  ],
}

export const PowerDevicesData = {
  pantograph: {
    front: {
      status: ON,
      isChangeable: true
    },
    rear: {
      status: OFF,
      isChangeable: true
    }
  },

  earthingSwitch3kV: {
    front: {
      status: OFF,
      isChangeable: false
    },
    rear: {
      status: OFF,
      isChangeable: false
    }
  },

  earthingSwitch25kV: {
    front: {
      status: OFF,
      isChangeable: false
    },
    rear: {
      status: OFF,
      isChangeable: false
    }
  },

  BV: {
    front: {
      status: ON,
      isChangeable: true
    },
    rear: {
      status: ON,
      isChangeable: true
    }
  },

  GV: {
    front: {
      status: ON,
      isChangeable: true
    },
    rear: {
      status: ON,
      isChangeable: true
    }
  },

  externalPower3kV: {
    front: {
      status: OFF,
      isChangeable: false
    },
    rear: {
      status: OFF,
      isChangeable: false
    }
  },

  disconnector3kV: {
    front: {
      status: OFF,
      isChangeable: true
    },
    rear: {
      status: OFF,
      isChangeable: true
    }
  },

  disconnector25kV: {
    center: {
      status: OFF,
      isChangeable: true
    }
  },

  TP1: {
    front: {
      status: ON,
      isChangeable: true
    },
    rear: {
      status: ON,
      isChangeable: true
    }
  },

  TP2: {
    front: {
      status: ON,
      isChangeable: true
    },
    rear: {
      status: ON,
      isChangeable: true
    }
  },

  PSN: {
    front: {
      status: ON,
      isChangeable: true
    },
    rear: {
      status: ON,
      isChangeable: true
    }
  },

  disconnector380: {
    center: {
      status: ON,
      isChangeable: true
    }
  },

  onBoardNetwork110: {
    front: {
      status: ON,
      isChangeable: false
    },
    rear: {
      status: ON,
      isChangeable: false
    }
  },

  onBoardNetwork380: {
    front: {
      status: ON,
      isChangeable: false
    },
    rear: {
      status: ON,
      isChangeable: false
    }
  },

  auxiliaryCompressor: {
    front: {
      status: ON,
      isChangeable: false
    },
    rear: {
      status: ON,
      isChangeable: false
    }
  },

  airPressureCC: {
    front: {
      status: OFF,
      isChangeable: false,
      value: 0.935
    },
    rear: {
      status: OFF,
      isChangeable: false,
      value: 0.935
    }
  }
}

export const listPowerDevicesData = [
  {
    id: 'Токоприемники',
    data: {
      front: [PowerDevicesData.pantograph.front],
      rear: [PowerDevicesData.pantograph.rear]
    }
  },
  {
    id: 'Заземлитель 3 / 25 кВ',
    data: {
      front: [PowerDevicesData.earthingSwitch3kV.front, PowerDevicesData.earthingSwitch25kV.front],
      rear: [PowerDevicesData.earthingSwitch3kV.rear, PowerDevicesData.earthingSwitch25kV.rear]
    }
  },
  {
    id: 'БВ / ГВ',
    data: {
      front: [PowerDevicesData.BV.front, PowerDevicesData.GV.front],
      rear: [PowerDevicesData.BV.rear, PowerDevicesData.GV.rear],
    }
  },
  {
    id: 'Внешнее питание 3 кВ',
    data: {
      front: [PowerDevicesData.externalPower3kV.front],
      rear: [PowerDevicesData.externalPower3kV.rear]
    }

  },
  {
    id: 'Разъединитель 3 / 25 / 3 кВ',
    data: {
      front: [PowerDevicesData.disconnector3kV.front],
      center: [PowerDevicesData.disconnector25kV.center],
      rear: [PowerDevicesData.disconnector3kV.rear]
    }
  },
  {
    id: 'ТП',
    data: {
      front: [PowerDevicesData.TP1.front, PowerDevicesData.TP2.front],
      rear: [PowerDevicesData.TP1.rear, PowerDevicesData.TP2.rear]
    }
  },
  {
    id: 'ПСН',
    data: {
      front: [PowerDevicesData.PSN.front],
      rear: [PowerDevicesData.PSN.rear]
    }
  },
  {
    id: 'Разъединитель 380 В',
    data: {
      center: [PowerDevicesData.disconnector380.center]
    }
  },
  {
    id: 'Бортовая сеть 110 В',
    data: {
      front: [PowerDevicesData.onBoardNetwork110.front],
      rear: [PowerDevicesData.onBoardNetwork110.rear]
    }
  },
  {
    id: 'Бортовая сеть 380 В',
    data: {
      front: [PowerDevicesData.onBoardNetwork380.front],
      rear: [PowerDevicesData.onBoardNetwork380.rear]
    }
  },
  {
    id: 'Вспомогательный компрессор',
    data: {
      front: [PowerDevicesData.auxiliaryCompressor.front],
      rear: [PowerDevicesData.auxiliaryCompressor.rear]
    }
  },
  {
    id: 'Давление в ЦУ, МПа',
    data: {
      front: [PowerDevicesData.airPressureCC.front],
      rear: [PowerDevicesData.airPressureCC.rear]
    }
  }
]

export const BrakingDevices = {
  GK1: {
    status: OFF,
    isChangeable: true
  },

  GK2: {
    status: OFF,
    isChangeable: true
  },

  brakePneumatic: {
    status: OFF,
    isChangeable: false
  },

  brakeElectric: {
    status: OFF,
    isChangeable: false
  },

  brakeParking: {
    status: OFF,
    isChangeable: false
  },

  brakeEmergency: {
    status: OFF,
    isChangeable: false
  },

  pneumaticSpring: {
    status: ON,
    isChangeable: false
  },

  airPressureTM: {
    status: OFF,
    value: '0.500'
  },

  airPressurePM: {
    status: OFF,
    value: PowerDevicesData.airPressureCC.front.value
  }
}

export const listBrakingDevices = [
  {
    id: 'Главный компрессор',
    data: {
      center: [BrakingDevices.GK1, BrakingDevices.GK2]
    }
  },

  {
    id: 'Пневматический тормоз',
    data: {
      center: [BrakingDevices.brakePneumatic]
    }
  },

  {
    id: 'Электрический тормоз',
    data: {
      center: [BrakingDevices.brakeElectric],
    }
  },

  {
    id: 'Стояночный тормоз',
    data: {
      center: [BrakingDevices.brakeParking]
    }
  },

  {
    id: 'Стоп-кран',
    data: {
      center: [BrakingDevices.brakeEmergency]
    }
  },

  {
    id: 'Пневморессора',
    data: {
      center: [BrakingDevices.pneumaticSpring]
    }
  },

  {
    id: 'Давление в ТМ, МПа',
    data: {
      center: [BrakingDevices.airPressureTM]
    }
  },

  {
    id: 'Давление в ПМ, МПа',
    data: {
      center: [BrakingDevices.airPressurePM]
    }
  }

]

export const ClimateDevices = {
  cabinFront: {
    status: ON,
    isChangeable: true
  },

  wagon01: {
    status: ON,
    isChangeable: true,
  },

  wagon02: {
    status: ON,
    isChangeable: true,
  },

  wagon03: {
    status: ON,
    isChangeable: true,
  },

  wagon04: {
    status: ON,
    isChangeable: true,
  },

  wagon05: {
    status: ON,
    isChangeable: true,
  },

  cabinRear: {
    status: ON,
    isChangeable: true
  },
}

export const listClimateDevices = {
  cabin: {
    front: [ClimateDevices.cabinFront],
    rear: [ClimateDevices.cabinRear]
  },
  wagons: [
    {
      center:
        [ ClimateDevices.wagon01 ]
    },
    {
      center:
        [ ClimateDevices.wagon02 ]
    },
    {
      center:
        [ ClimateDevices.wagon03 ]
    },
    {
      center:
        [ ClimateDevices.wagon04 ]
    },
    {
      center:
        [ ClimateDevices.wagon05 ]
    },
  ]

}

export const Doors = {
  wagon01: {
    R1: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    R2: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L1: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L2: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
  },

  wagon02: {
    R1: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    R2: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L1: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L2: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
  },

  wagon03: {
    R1: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    R2: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L1: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L2: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
  },

  wagon04: {
    R1: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    R2: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L1: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L2: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
  },

  wagon05: {
    R1: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    R2: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L1: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L2: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
  }
}

export const listDoorsData = {
  wagons: [
    {
      top: {
        front: [Doors.wagon01.R1],
        rear: [Doors.wagon01.R2]
      },

      bottom: {
        front: [Doors.wagon01.L1],
        rear: [Doors.wagon01.L2]
      },
    },
    {
      top: {
        front: [Doors.wagon02.R1],
        rear: [Doors.wagon02.R2]
      },

      bottom: {
        front: [Doors.wagon02.L1],
        rear: [Doors.wagon02.L2]
      },
    },
    {
      top: {
        front: [Doors.wagon03.R1],
        rear: [Doors.wagon03.R2]
      },

      bottom: {
        front: [Doors.wagon03.L1],
        rear: [Doors.wagon03.L2]
      },
    },
    {
      top: {
        front: [Doors.wagon04.R1],
        rear: [Doors.wagon04.R2]
      },
      bottom: {
        front: [Doors.wagon04.L1],
        rear: [Doors.wagon04.L2]
      },
    },
    {
      top: {
        front: [Doors.wagon05.R1],
        rear: [Doors.wagon05.R2]
      },
      bottom: {
        front: [Doors.wagon05.L1],
        rear: [Doors.wagon05.L2]
      },
    },
  ]
}

export const Axlebox = {
  wagon01: {
    R1: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    R2: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    R3: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    R4: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L1: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L2: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L3: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L4: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
  },

  wagon02: {
    R1: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    R2: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    R3: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    R4: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L1: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L2: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L3: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L4: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
  },

  wagon03: {
    R1: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    R2: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    R3: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    R4: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L1: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L2: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L3: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L4: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
  },

  wagon04: {
    R1: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    R2: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    R3: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    R4: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L1: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L2: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L3: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L4: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
  },

  wagon05: {
    R1: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    R2: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    R3: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    R4: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L1: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L2: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L3: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
    L4: {
      status: ON,
      isChangeable: true,
      rectangle: true
    },
  },
}

export const listAxlebox = {
  wagons: [
    {
      top: {
        front: [
          Axlebox.wagon01.R1,
          Axlebox.wagon01.R2,
        ],
        rear: [
          Axlebox.wagon01.R3,
          Axlebox.wagon01.R4,
        ]
      },
      bottom: {
        front: [
          Axlebox.wagon01.R1,
          Axlebox.wagon01.R2,
        ],
        rear: [
          Axlebox.wagon01.R3,
          Axlebox.wagon01.R4,
        ]
      },
    },
    {
      top: {
        front: [
          Axlebox.wagon02.R1,
          Axlebox.wagon02.R2,
        ],
        rear: [
          Axlebox.wagon02.R3,
          Axlebox.wagon02.R4,
        ]
      },
      bottom: {
        front: [
          Axlebox.wagon02.R1,
          Axlebox.wagon02.R2,
        ],
        rear: [
          Axlebox.wagon02.R3,
          Axlebox.wagon02.R4,
        ]
      },
    },
    {
      top: {
        front: [
          Axlebox.wagon03.R1,
          Axlebox.wagon03.R2,
        ],
        rear: [
          Axlebox.wagon03.R3,
          Axlebox.wagon03.R4,
        ]
      },
      bottom: {
        front: [
          Axlebox.wagon03.R1,
          Axlebox.wagon03.R2,
        ],
        rear: [
          Axlebox.wagon03.R3,
          Axlebox.wagon03.R4,
        ]
      },
    },
    {
      top: {
        front: [
          Axlebox.wagon04.R1,
          Axlebox.wagon04.R2,
        ],
        rear: [
          Axlebox.wagon04.R3,
          Axlebox.wagon04.R4,
        ]
      },
      bottom: {
        front: [
          Axlebox.wagon04.R1,
          Axlebox.wagon04.R2,
        ],
        rear: [
          Axlebox.wagon04.R3,
          Axlebox.wagon04.R4,
        ]
      },
    },
    {
      top: {
        front: [
          Axlebox.wagon05.R1,
          Axlebox.wagon05.R2,
        ],
        rear: [
          Axlebox.wagon05.R3,
          Axlebox.wagon05.R4,
        ]
      },
      bottom: {
        front: [
          Axlebox.wagon05.R1,
          Axlebox.wagon05.R2,
        ],
        rear: [
          Axlebox.wagon05.R3,
          Axlebox.wagon05.R4,
        ]
      },
    },
  ]
}

export const listLegendAxle = [
  {
    status: ON,
    isChangeable: false,
    description: 'Рабочая температура'
  },
  {
    status: WARNING,
    isChangeable: false,
    description: 'Температура более 100 С'
  },
  {
    status: ERROR,
    isChangeable: false,
    description: 'Температура более 120 С'
  },
  {
    status: BLOCK,
    isChangeable: false,
    description: 'Датчик контроля нагрева букс заблокирован'
  },
  {
    status: MALFUNCTION,
    isChangeable: false,
    description: 'Неисправность датчика'
  }
]

export const Settings = {
  washing: {
    status: OFF,
    isChangeable: true
  },

  stanby: {
    status: OFF,
    isChangeable: true
  },

  testTP: {
    status: OFF,
    isChangeable: true
  },

  modeOperating: {
    status: OFF,
    isChangeable: true
  },

  modeForwarding: {
    status: OFF,
    isChangeable: true
  },

  circuitTwoWire: {
    status: OFF,
    isChangeable: true
  },

  modeRegularClimate: {
    status: OFF,
    isChangeable: true
  },

  urban: {
    status: OFF,
    isChangeable: true
  },

  longDistance: {
    status: CHECKED,
    isChangeable: true,
  }
}

export const listSettings = [
  {
    ...Settings.washing,
    name: 'Проезд через мойку'  
  },
  {...Settings.stanby,
    name: 'Режим отстоя'
  },
  {...Settings.testTP,
    name: 'Режим тестирования ТП'
  },
  {...Settings.modeOperating,
    name: 'Режим питания от локомотива "Эксплуатация" '
  },
  {...Settings.modeForwarding,
    name: 'Режим питания от локомотива "Пересылка"'
  },
  {...Settings.circuitTwoWire,
    name:'Питание от локомотива - двухпроводная схема'
  },
  {...Settings.modeRegularClimate,
    name: 'Питание от локомотива - штатный режим КУ'
  },
  {...Settings.urban,
    name: 'Пригородное / городское сообщение'
  },
  {...Settings.longDistance,
    name: 'Дальнее сообщение'
  }
]

export const navPanelSettings = [
  {
    name: 'Принять',
    button: 'Enter'
  },
  {
    name: 'Вверх',
    button: '↑'
  },
  {
    name: 'Вниз',
    button: '↓'
  },
  {
    name: 'Включить',
    button: '+'
  },
  {
    name: 'Выключить',
    button: '-'
  },
  {},
  {},
  {},
  {},
  {}
];