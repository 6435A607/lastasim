export const ON = 'ON';
export const OFF = 'OFF';
export const ERROR = 'ERROR';
export const WARNING = 'WARNING';
export const BLOCK = 'BLOCK';
export const DISAPPEAR = 'DISAPPEAR';
export const MALFUNCTION = 'MALFUNCTION';
export const CHECKED = 'CHECKED';