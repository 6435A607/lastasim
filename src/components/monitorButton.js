import React from 'react';
import { withRouter, generatePath } from 'react-router-dom';
import { listDisplays, screens } from './redux/dataScreen';
import styles from './styleMonitor.module.css';

function MonitorButton(props) {
  let idDisplay = props.location.pathname.replaceAll('/display/', '');
  let href = null;
  if(listDisplays[idDisplay]) href = listDisplays[idDisplay][props.href].id;
  const handleOnClick = (e) => {
      if (href === screens['Legend'].id) {
        alert(href);
        return;
      }

      //На изображении экрана кнопка может быть без ссылки (пустая черная кнопка). У нее ссылка null. 
      //На нее реагировать не надо.
      if(href != null) { 
        props.history.push(generatePath('/display/:id', {
          id: href
        }));
      }
  }

  return (
    <div className={styles.monitorButton} onClick={handleOnClick}>
      {props.label}
    </div>
  )
}

MonitorButton.defaultProps = {
  href: 0 //Для кнопок на боковых панелях, чтобы конфилкта не было с поиском ссылок
}

export default withRouter(MonitorButton);