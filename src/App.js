import './App.css';
import Monitor from './components/monitor';
import React, { Suspense } from 'react';
import { HashRouter as Router } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <header className="App-header">
      </header>
      <Router>
        <Suspense fallback={<div>Loading...</div>}>
          <Monitor />
        </Suspense>
      </Router>
    </div>
  );
}

export default App; 
